import "./Button.scss";

export default function Button({ children, outline, onClick }) {
  return (
    <button onClick={onClick} className={"button " + (outline ? "button_outline" : "")}>
      {children}
    </button>
  );
}
