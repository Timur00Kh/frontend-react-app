import { Link } from "react-router-dom";
import Button from "../Button/Button";
import "./Form.scss";

export default function Form({header, buttonText, linkText, linkTo}) {

  
  return (
    <form className="form">
      <Link to={linkTo} className="form__link">
        {linkText}
      </Link>
      <h1 className="form__header">{header}</h1>
      <fieldset className="form__fieldset">
        <input placeholder="Логин" type="text" className="form__input" />
      </fieldset>
      <fieldset className="form__fieldset">
        <input placeholder="Пароль" type="password" className="form__input" />
      </fieldset>
      <fieldset className="form__fieldset form__checkbox-wrap">
        <input type="checkbox" className="form__checkbox" />
        <span className="form__checkbox-text">
          Я согласен получать обновления на почту
        </span>
      </fieldset>
      <div className="form__btn-wrap">
        <Button>{buttonText}</Button>
      </div>
    </form>
  );
}
