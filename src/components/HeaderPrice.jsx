import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { useTotalCartPrice } from "../hooks/useTotalCartPrice";

export default function HeaderPrice() {
  const cart = useSelector((state) => state.cart);
  const cartTotalPrice = useTotalCartPrice();

  return (
    <div className="header__cart-block">
      <Link to="/cart">
        <span>
          {cart.length} товара <br /> на сумму {cartTotalPrice} ₽
        </span>
      </Link>
    </div>
  );
}
