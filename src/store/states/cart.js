import { createSlice } from '@reduxjs/toolkit'
import { nanoid } from 'nanoid' 

const CART_STATE = 'CART_STATE';
const initialCart = localStorage.getItem(CART_STATE);
const initialState = initialCart ? JSON.parse(initialCart) : [];

export const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        addToCart: (state, action) => {
            const product = {
                ...action.payload,
                cart_id: nanoid()
            }
            state.push(product);
        },
        removeFromCart: (state, action) => {
            const newState = state.filter((product) => product.cart_id !== action.payload);
            return newState;
        },
    }
})

export const { addToCart, removeFromCart } = cartSlice.actions;

export default cartSlice.reducer;

export const cartMiddleware = (store) => (next) => (action) => {
    let result = next(action);

    if (action.type.startsWith('cart/')) {
        const {cart} = store.getState();
        localStorage.setItem(CART_STATE, JSON.stringify(cart));
    }
    return result;
}
