import { configureStore } from '@reduxjs/toolkit'
import cart, { cartMiddleware } from './states/cart';

const logger = (store) => (next) => (action) => {
  console.log(store.getState());
  const result = next(action);
  console.log(store.getState());
  return result;
}

export const store = configureStore({
  reducer: {
    cart
  },
  middleware: [cartMiddleware]
})

