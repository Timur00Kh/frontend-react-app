import classes from "./ProductPage.module.scss";
import styled from "styled-components";
import image from "./ppimg.png";
import { Button as MaterialButton } from "@mui/material";
import Button from "../../components/Button/Button";
import { useNavigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { productList } from "../../utils/data";
import HeaderPrice from "../../components/HeaderPrice";

const Image = styled.img`
  width: 100%;
  height: auto;
  max-width: 500px;
`;

const Title = styled.h1`
  font-style: normal;
  font-weight: 500;
  font-size: 30px;
  line-height: 37px;
  color: #d58c51;
`;

const Text = styled.p`
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 17px;
  color: #ffffff;
`;

const Price = styled.span`
  font-style: normal;
  font-weight: 500;
  font-size: 23px;
  line-height: 28px;
  color: #ffffff;
  white-space: pre;
`;

const Weight = styled.span`
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 22px;
  color: #ffffff;
  white-space: pre;
`;

const Divider = styled.span`
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 22px;
  color: #ffffff;

  margin: 0 8px;
`;

const HeaderButton = ({onClick, children}) => (
  <MaterialButton onClick={onClick} sx={{ border: "1px solid #d58c51", color: "#d58c51" }}>
    {children}
  </MaterialButton>
);

export default function ProductPage() {
  const { productId } = useParams();
  const [product, setProduct] = useState({});


  useEffect(() => {
    const p = productList.find((e) => e.id === Number(productId));
    if (p) {
      setProduct(p);
    } else {
      // redirect 404
    }
  }, [productId]);


  const navigate = useNavigate();
  const back = () => navigate(-1);

  return (
    <div className={classes.productPage}>
      <header className={classes.productPage__header}>
        <HeaderButton onClick={back} variant="outlined">{'<=='}</HeaderButton>
        <HeaderPrice />
      </header>
      <main className={classes.productPage__main}>
        <div
          style={{
            marginRight: "100px",
          }}
        >
          <Image src={product.image} />
        </div>
        <div>
          <Title>{product.title}</Title>
          <Text>{product.text}</Text>
          <div className={classes.priceWrap}>
            <Price>{product.price?.toLocaleString('ru')} ₽</Price>
            <Divider>/</Divider>
            {Boolean(product.weight) && <Weight>{product.weight} г.</Weight>}
            {Boolean(product.amount) && <Weight>{product.amount} шт.</Weight>}
            <div className={classes.btnWrap}>
              <Button>В корзину</Button>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
