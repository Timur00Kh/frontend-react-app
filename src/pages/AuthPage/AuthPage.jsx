import Form from "../../components/Form/Form";
import "./AuthPage.scss";

export default function AuthPage({ singIn, signUp }) {
  return (
    <div className="auth-page">
      {singIn && (
        <Form
          header={"вход"}
          linkText="Зарегистрироваться"
          linkTo="/signUp"
          buttonText={"Войти"}
        />
      )}
      {signUp && (
        <Form
          header={"Регистрация"}
          linkText="Авторизоваться"
          linkTo="/signIn"
          buttonText={"Зарегистрироваться"}
        />
      )}
    </div>
  );
}
