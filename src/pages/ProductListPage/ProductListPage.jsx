import "./ProductListPage.scss";
import { productList } from "../../utils/data";
import ProductCard from "../../components/ProductCard/ProductCard";
import { useSelector, useDispatch } from "react-redux";
import { addToCart } from "../../store/states/cart";
import { Link } from "react-router-dom";
import { useTotalCartPrice } from "../../hooks/useTotalCartPrice";
import HeaderPrice from "../../components/HeaderPrice";

export default function ProductListPage() {
  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();

  const handleAddToCart = (product) => {
    dispatch(addToCart(product));
  };

  return (
    <div>
      <header className="header">
        <h1 className="header__title">наша продукция</h1>
        <HeaderPrice/>
      </header>
      <main className="product-list-container">
        {productList.map((product) => (
          <div key={product.id} className="product-list-item">
            <ProductCard
              id={product.id}
              title={product.title}
              text={product.text}
              image={product.image}
              price={product.price}
              weight={product.weight}
              amount={product.amount}
              onButtonClick={() => handleAddToCart(product)}
            />
          </div>
        ))}
      </main>
    </div>
  );
}
