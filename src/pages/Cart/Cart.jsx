import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Button from "../../components/Button/Button";
import HeaderPrice from "../../components/HeaderPrice";
import { removeFromCart } from "../../store/states/cart";

export function Cart() {
  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleRemoveFromCart = (id) => {
    dispatch(removeFromCart(id));
  };

  const back = () => navigate(-1);

  return (
    <main style={{ color: "white" }}>
      <div style={{ display: "flex", padding: "50px" }}>
        <Button onClick={back}>{"<=="}</Button>
        <HeaderPrice />
      </div>
      {cart.map((product, i) => (
        <div key={i}>
          <img src={product.image} alt="" />
          <span>{product.title}</span>
          <span>{product.price}</span>
          {/* <pre style={{ color: "white" }}>
            {JSON.stringify(product, null, 2)}
          </pre> */}
          <Button onClick={() => handleRemoveFromCart(product.cart_id)}>
            Удалить
          </Button>
        </div>
      ))}
    </main>
  );
}
