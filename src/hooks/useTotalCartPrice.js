import { useMemo } from "react";
import { useSelector } from "react-redux";

export function useTotalCartPrice() {
  const cart = useSelector((state) => state.cart);

  const totalPrice = useMemo(
    () => cart.reduce((sum, product) => sum + product.price, 0),
    [cart]
  );

  return totalPrice;
}
