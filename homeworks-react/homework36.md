
## Инструкция по выполнению

- Переключиться в основную ветку проекта `git checkout main`
- Скачать изменения из основной ветки `git pull`
- Создать новую ветку в своём проекте `git branch homework-N`, где `N` - номер домашки
- Переключиться в данную ветку `git checkout homework-N`
- Выполнить основную задачу домашнего задания,отредактировав необходимые файлы
- Добавить изменения в индекс Git `git add .`
- Сделать коммит изменений `git commit -m “Ваше сообщение”`
- Запушить изменения в Gitlab `git push origin homework-N`
- Создать новый merge request в Gitlab (через интерфейс насайте)
- Прикрепить ссылку на merge request и архив в систему обучения для проверки наставником
- После проверки наставника вливаем merge request в основную ветку проекта (нажимаем кнопку merge)

## 36. Hooks. useState, useEffect. Роутинг

### Задача

Добавить роутинг в приложение

### Критерии приёмки 

- Использовать роутер последней версии
- Добавить динамический роутинг на страницы продукта

### Команды

- `npm install react-router-dom` - Установка пакета React Router


### Инструкция по выполнению

- [Инструкция по выполнению](#инструкция-по-выполнению)
- Ветка: `homework-36`
